package neto.nestedpropertybuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author valdemar.arantes
 */
@WebServlet(urlPatterns = {"/NestedPropertyBuilder"})
public class NewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * <p>
     * @param request servlet request
     * @param response servlet response
     * <p>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String expr = request.getParameter("expr");

        String ret = "";

        if (StringUtils.isNotBlank(expr)) {
            ret = expr;
        }

        String[] unitExprArr = StringUtils.split(expr, ".");

        ret = "";
        for (String unitExpr : unitExprArr) {
            if (StringUtils.isBlank(unitExpr)) {
                continue;
            }
            if (StringUtils.startsWith(unitExpr, "get")) {

                // Remove o get
                String unitExprWithoutGet = StringUtils.removeStart(unitExpr, "get");

                // Remove os parênteses
                unitExprWithoutGet = StringUtils.removeEnd(unitExprWithoutGet, "()");

                char firstChar = unitExprWithoutGet.charAt(0);
                if (firstChar == '(') {
                    String aux = StringUtils.remove(unitExprWithoutGet, "(");
                    String index = StringUtils.remove(aux, ")");
                    ret = ret + "[" + index + "]";
                } else {

                    // Se todas maiúsculas, não descapitalizo a primeira letra
                    if (StringUtils.isAllUpperCase(unitExprWithoutGet)) {
                        if (ret.equals("")) {
                            ret = unitExprWithoutGet;
                        } else {
                            ret = ret + "." + unitExprWithoutGet;
                        }
                    } else {

                        // Descapitaliza a primeira letra (depois do get!)
                        String firstCharLowerCase = new String(new char[]{firstChar}).toLowerCase();

                        if (ret.equals("")) {
                            ret = firstCharLowerCase + unitExprWithoutGet.substring(1);
                        } else {
                            ret = ret + "." + firstCharLowerCase + unitExprWithoutGet.substring(1);
                        }
                    }
                }
            } else {
                ret += unitExpr;
            }
        }

        try (PrintWriter out = response.getWriter()) {
            out.println(ret);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * <p>
     * @param request servlet request
     * @param response servlet response
     * <p>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * <p>
     * @param request servlet request
     * @param response servlet response
     * <p>
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * <p>
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
